#include <Arduino.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include "pins.h"

LiquidCrystal lcd(LCD_RS,LCD_EN,LCD_D4,LCD_D5,LCD_D6,LCD_D7);
int speed=0;

void setup();
void loop();
void setspeed();

void setup() 
{
  pinMode(Key_Enter, INPUT_PULLUP);
  pinMode(Key_Up, INPUT_PULLUP);
  pinMode(Key_Down, INPUT_PULLUP);
  pinMode(Key_Left, INPUT_PULLUP);
  pinMode(Key_Right, INPUT_PULLUP);

  // pinMode(MS1, OUTPUT);
  // pinMode(MS2, OUTPUT);
  // pinMode(MS3, OUTPUT);
  // pinMode(RESET, OUTPUT);

  DDRB=DDRB|B01111000; // make sure pin 9 is set up as an output (it's usually a crystal pin)

  pinMode(STEP, OUTPUT);
  pinMode(DIR, OUTPUT);

  // PWM settings
  // TCCR0A=0;//reset the register
  // TCCR0B=0;//reset tthe register
  // TCCR0A=0b01010011;// fast pwm mode
  // TCCR0B=0b00001011;// prescaler 64, WGM02=1
  // OCR0A=77;//duty cycle for pin 6
  
  // TCCR0A=B01<<6|B11;
  // TCCR0B=B1<<3|B011;
  // OCR0A=100;

  lcd.begin(16,2);
  lcd.clear();
  lcd.print(" StepperTester");
  lcd.setCursor(0,1);
  lcd.print("     v0.1");
  delay(3000);
  lcd.clear();

  setspeed();
  digitalWrite(RESET, HIGH);

}

void setspeed()
{
  // digitalWrite(RESET, LOW);

  // digitalWrite(MS1, ((speed&1)!=0)?HIGH:LOW);
  // digitalWrite(MS2, ((speed&2)!=0)?HIGH:LOW);
  // digitalWrite(MS3, ((speed&4)!=0)?HIGH:LOW);

  int mask=(speed&6)<<4|(speed&1)<<3;
  PORTB=PORTB&B10010111|mask;

  // delay(10);
  // digitalWrite(RESET, HIGH);
  lcd.setCursor(15,1);
  lcd.print(speed);
}

void loop() 
{
  lcd.setCursor(0,0);
  lcd.print(!digitalRead(Key_Enter)?"Enter":"     ");
  if (!digitalRead(Key_Enter))
    digitalWrite(STEP,LOW);
  
  lcd.setCursor(6,0);
  lcd.print(!digitalRead(Key_Up)?"Up":"  ");
  if (!digitalRead(Key_Up))
  {
    speed--;
    if (speed<0)
      speed=0;
    setspeed();
  }

  lcd.setCursor(9,0);
  lcd.print(!digitalRead(Key_Down)?"Down":"    ");
  if (!digitalRead(Key_Down))
  {
    speed++;
    if (speed>7)
      speed=7;
    setspeed();
  }

  lcd.setCursor(0,1);
  lcd.print(!digitalRead(Key_Left)?"Left":"    ");
  if (!digitalRead(Key_Left))
  {
    digitalWrite(DIR,LOW);
    analogWrite(STEP,128);
  }

  lcd.setCursor(5,1);
  lcd.print(!digitalRead(Key_Right)?"Right":"     ");
  if (!digitalRead(Key_Right))
  {
    digitalWrite(DIR,HIGH);
    analogWrite(STEP,128);
  }

  delay(250);
}
#pragma once

/*************************************************
 * Public Constants to convert Atmega pin numbers
 * to Arduino IDE pin numbers
 * Means no need to manually convert the pins
 *************************************************/

// Defintion for the Digital Pins
#define Atmega_P2    0  // RX
#define Atmega_P3    1  // TX
#define Atmega_P4    2
#define Atmega_P5    3  // PWM
#define Atmega_P6    4
#define Atmega_P9    PB6 // usually a crystal pin
#define Atmega_P11   5  // PWM
#define Atmega_P12   6  // PWM
#define Atmega_P13   7
#define Atmega_P14   8
#define Atmega_P15   9  // PWM
#define Atmega_P16  10  // PWM
#define Atmega_P17  11  // PWM
#define Atmega_P18  12
#define Atmega_P19  13

// Definition for Analog Pins
#define Atmega_P23  A0
#define Atmega_P24  A1
#define Atmega_P25  A2
#define Atmega_P26  A3
#define Atmega_P27  A4
#define Atmega_P28  A5

// assignments for this board

#define LCD_RS Atmega_P6
#define LCD_EN Atmega_P11
#define LCD_D4 Atmega_P2 // v0.2
#define LCD_D5 Atmega_P3 // v0.2
#define LCD_D6 Atmega_P4 // v0.2
#define LCD_D7 Atmega_P5 // v0.2

#define Key_Up    Atmega_P23
#define Key_Down  Atmega_P24
#define Key_Left  Atmega_P25
#define Key_Right Atmega_P26
#define Key_Enter Atmega_P27

#define MOSI  Atmega_P17
#define MISO  Atmega_P18
#define SCK   Atmega_P19
#define CS    Atmega_P9
#define DIAG1 Atmega_P10

#define STEP  Atmega_P12
#define DIR   Atmega_P13
#define MS1   Atmega_P17
#define MS2   Atmega_P19
#define MS3   Atmega_P9
#define RESET Atmega_P18

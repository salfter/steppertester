Stepper Motor/Driver Tester
===========================

I'm in the middle of building a 3D printer from scratch, and found that
something like this would be useful to check the mechanics without wiring in
a full control board.  I've lashed something together with an Arduino Uno, a
DRV8825 board, and a breadboard...you can think of this as a more permanent
version.

I also had a driver board in another printer crap out recently; something to
verify proper operation of the driver board would've been useful.

I plan on having this device support pretty much any Pololu-compatible
stepper-motor driver board, but with an emphasis on TMC2130 support so that
you can determine the optimal settings to burn into your printer firmware.

With nothing but through-hole components, this should be simple for anybody
to build.  The only somewhat specialized bit of equipment you'll need is a
USBasp to program the microcontroller (or maybe you can lash up an Arduino
to do the job).

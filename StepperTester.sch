EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "StepperTester"
Date "2018-12-24"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pololu:POLOLU U2
U 1 1 5C21EDD9
P 2550 6350
F 0 "U2" H 2100 6950 60  0000 C CNN
F 1 "POLOLU" H 2850 5750 60  0000 C CNN
F 2 "pololu:pololu" H 2550 6350 60  0001 C CNN
F 3 "http://suddendocs.samtec.com/catalog_english/ssa.pdf" H 2550 6350 60  0001 C CNN
F 4 "CONN RCPT 17POS 0.1 GOLD PCB" H 0   0   50  0001 C CNN "Description"
F 5 "Samtec Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 6 "SSA-117-S-G" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "SAM1121-17-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=SAM1121-17-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "break into 3 pieces: 8, 8, 1" H 0   0   50  0001 C CNN "Note"
	1    2550 6350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5C21F58C
P 3650 6050
F 0 "J2" H 3730 6042 50  0000 L CNN
F 1 "Conn_01x04" H 3730 5951 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3650 6050 50  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 3650 6050 50  0001 C CNN
F 4 "CONN HEADER VERT 4POS 2.54MM" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PRPC004SAAN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "S1011EC-04-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=S1011EC-04-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3650 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 5950 3450 5950
Wire Wire Line
	3350 6050 3450 6050
Wire Wire Line
	3350 6150 3450 6150
Wire Wire Line
	3350 6250 3450 6250
$Comp
L power:+5V #PWR0101
U 1 1 5C21F693
P 1450 5700
F 0 "#PWR0101" H 1450 5550 50  0001 C CNN
F 1 "+5V" H 1465 5873 50  0000 C CNN
F 2 "" H 1450 5700 50  0001 C CNN
F 3 "" H 1450 5700 50  0001 C CNN
	1    1450 5700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5C21F6C1
P 1700 1450
F 0 "#PWR0102" H 1700 1300 50  0001 C CNN
F 1 "+5V" H 1715 1623 50  0000 C CNN
F 2 "" H 1700 1450 50  0001 C CNN
F 3 "" H 1700 1450 50  0001 C CNN
	1    1700 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1450 1700 1500
$Comp
L power:GND #PWR0103
U 1 1 5C21F777
P 3400 6950
F 0 "#PWR0103" H 3400 6700 50  0001 C CNN
F 1 "GND" H 3405 6777 50  0000 C CNN
F 2 "" H 3400 6950 50  0001 C CNN
F 3 "" H 3400 6950 50  0001 C CNN
	1    3400 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 6950 3400 6850
Wire Wire Line
	3400 6750 3350 6750
Wire Wire Line
	3350 6850 3400 6850
Connection ~ 3400 6850
Wire Wire Line
	3400 6850 3400 6750
$Comp
L power:GND #PWR0104
U 1 1 5C21F912
P 1700 4600
F 0 "#PWR0104" H 1700 4350 50  0001 C CNN
F 1 "GND" H 1705 4427 50  0000 C CNN
F 2 "" H 1700 4600 50  0001 C CNN
F 3 "" H 1700 4600 50  0001 C CNN
	1    1700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 4550 1700 4600
Text GLabel 2300 2250 2    50   Input ~ 0
MISO
Text GLabel 2300 2150 2    50   Output ~ 0
MOSI
Text GLabel 2300 2350 2    50   Output ~ 0
SCK
$Comp
L power:GND #PWR0105
U 1 1 5C2A3A6E
P 5650 3050
F 0 "#PWR0105" H 5650 2800 50  0001 C CNN
F 1 "GND" H 5655 2877 50  0000 C CNN
F 2 "" H 5650 3050 50  0001 C CNN
F 3 "" H 5650 3050 50  0001 C CNN
	1    5650 3050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5C2A3B91
P 5650 1350
F 0 "#PWR0106" H 5650 1200 50  0001 C CNN
F 1 "+5V" H 5665 1523 50  0000 C CNN
F 2 "" H 5650 1350 50  0001 C CNN
F 3 "" H 5650 1350 50  0001 C CNN
	1    5650 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT_US R1
U 1 1 5C2A3CAA
P 6250 1600
F 0 "R1" H 6183 1554 50  0000 R CNN
F 1 "10k" H 6183 1645 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Piher_PT-10-V10_Vertical" H 6250 1600 50  0001 C CNN
F 3 "https://www.piher.net/pdf/12-PT10v03.pdf" H 6250 1600 50  0001 C CNN
F 4 "POT 10K OHM LINEAR" H -50 -450 50  0001 C CNN "Description"
F 5 "Piher" H -50 -450 50  0001 C CNN "Manufacturer"
F 6 "PT10LV10-103A2020-S" H -50 -450 50  0001 C CNN "Mfr PN"
F 7 "1" H -50 -450 50  0001 C CNN "Qty Per Unit"
F 8 "1993-1116-ND" H -50 -450 50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=1993-1116-ND" H -50 -450 50  0001 C CNN "Vendor 1 URL"
	1    6250 1600
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 5C2A3DD9
P 6250 1350
F 0 "#PWR0107" H 6250 1200 50  0001 C CNN
F 1 "+5V" H 6265 1523 50  0000 C CNN
F 2 "" H 6250 1350 50  0001 C CNN
F 3 "" H 6250 1350 50  0001 C CNN
	1    6250 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5C2A3DEE
P 6250 2050
F 0 "#PWR0108" H 6250 1800 50  0001 C CNN
F 1 "GND" H 6255 1877 50  0000 C CNN
F 2 "" H 6250 2050 50  0001 C CNN
F 3 "" H 6250 2050 50  0001 C CNN
	1    6250 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R2
U 1 1 5C2A42CD
P 6550 1600
F 0 "R2" H 6618 1646 50  0000 L CNN
F 1 "220" H 6618 1555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6590 1590 50  0001 C CNN
F 3 "https://www.seielect.com/Catalog/SEI-RNF_RNMF.pdf" H 6550 1600 50  0001 C CNN
F 4 "RES 220 OHM 1/4W 1% AXIAL" H -50 -450 50  0001 C CNN "Description"
F 5 "Stackpole" H -50 -450 50  0001 C CNN "Manufacturer"
F 6 "RNMF14FTC220R" H -50 -450 50  0001 C CNN "Mfr PN"
F 7 "1" H -50 -450 50  0001 C CNN "Qty Per Unit"
F 8 "S220CACT-ND" H -50 -450 50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=S220CACT-ND" H -50 -450 50  0001 C CNN "Vendor 1 URL"
	1    6550 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5C2A48E3
P 4850 1700
F 0 "#PWR0109" H 4850 1450 50  0001 C CNN
F 1 "GND" H 4855 1527 50  0000 C CNN
F 2 "" H 4850 1700 50  0001 C CNN
F 3 "" H 4850 1700 50  0001 C CNN
	1    4850 1700
	1    0    0    -1  
$EndComp
Text GLabel 5250 1800 0    50   Input ~ 0
LCD_RS
Text GLabel 5250 1600 0    50   Input ~ 0
LCD_EN
Text GLabel 5250 2500 0    50   Input ~ 0
LCD_D4
Text GLabel 5250 2600 0    50   Input ~ 0
LCD_D5
Text GLabel 5250 2700 0    50   Input ~ 0
LCD_D6
Text GLabel 5250 2800 0    50   Input ~ 0
LCD_D7
Text GLabel 2300 3550 2    50   Output ~ 0
LCD_D4
Text GLabel 2300 3650 2    50   Output ~ 0
LCD_D5
Text GLabel 2300 3750 2    50   Output ~ 0
LCD_D6
Text GLabel 2300 3850 2    50   Output ~ 0
LCD_D7
Text GLabel 2300 3950 2    50   Output ~ 0
LCD_RS
Text GLabel 2300 4050 2    50   Output ~ 0
LCD_EN
Text GLabel 2300 4150 2    50   Output ~ 0
STEP
Text GLabel 2300 4250 2    50   Output ~ 0
DIR
Text GLabel 1750 6150 0    50   Input ~ 0
STEP
Text GLabel 1750 6250 0    50   Input ~ 0
DIR
$Comp
L power:+12V #PWR0110
U 1 1 5C2A4EB7
P 3350 5450
F 0 "#PWR0110" H 3350 5300 50  0001 C CNN
F 1 "+12V" H 3365 5623 50  0000 C CNN
F 2 "" H 3350 5450 50  0001 C CNN
F 3 "" H 3350 5450 50  0001 C CNN
	1    3350 5450
	1    0    0    -1  
$EndComp
Text GLabel 2300 2450 2    50   Output ~ 0
CS
Text GLabel 1750 6550 0    50   Input ~ 0
CS
Text GLabel 1750 6450 0    50   Input ~ 0
SCK
Text GLabel 1750 6350 0    50   Input ~ 0
MOSI
Text GLabel 1750 6750 0    50   Output ~ 0
MISO
Text GLabel 3350 6550 2    50   Output ~ 0
DIAG1
Wire Wire Line
	1750 6850 1450 6850
Wire Wire Line
	1450 5850 1750 5850
Wire Wire Line
	1450 5850 1450 5700
Wire Wire Line
	1450 5850 1450 6850
$Comp
L power:GND #PWR0111
U 1 1 5C2A5B00
P 1350 5950
F 0 "#PWR0111" H 1350 5700 50  0001 C CNN
F 1 "GND" H 1355 5777 50  0000 C CNN
F 2 "" H 1350 5950 50  0001 C CNN
F 3 "" H 1350 5950 50  0001 C CNN
	1    1350 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 5950 1750 5950
Text GLabel 2300 2550 2    50   Input ~ 0
DIAG1
$Comp
L Switch:SW_Push SW2
U 1 1 5C2A6F68
P 5900 3900
F 0 "SW2" H 5900 4093 50  0000 C CNN
F 1 "SW_Push" H 5900 4094 50  0001 C CNN
F 2 "switches:SW_6x6_MULTI" H 5900 4100 50  0001 C CNN
F 3 "https://www3.panasonic.biz/ac/e_download/control/switch/light-touch/catalog/sw_lt_eng_5n.pdf" H 5900 4100 50  0001 C CNN
F 4 "SWITCH TACTILE SPST-NO 0.02A 15V" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "EVQ-PAG04M" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5900 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5C2A7064
P 5900 4350
F 0 "SW3" H 5900 4543 50  0000 C CNN
F 1 "SW_Push" H 5900 4544 50  0001 C CNN
F 2 "switches:SW_6x6_MULTI" H 5900 4550 50  0001 C CNN
F 3 "https://www3.panasonic.biz/ac/e_download/control/switch/light-touch/catalog/sw_lt_eng_5n.pdf" H 5900 4550 50  0001 C CNN
F 4 "SWITCH TACTILE SPST-NO 0.02A 15V" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "EVQ-PAG04M" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5900 4350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5C2A7098
P 5900 4800
F 0 "SW4" H 5900 4993 50  0000 C CNN
F 1 "SW_Push" H 5900 4994 50  0001 C CNN
F 2 "switches:SW_6x6_MULTI" H 5900 5000 50  0001 C CNN
F 3 "https://www3.panasonic.biz/ac/e_download/control/switch/light-touch/catalog/sw_lt_eng_5n.pdf" H 5900 5000 50  0001 C CNN
F 4 "SWITCH TACTILE SPST-NO 0.02A 15V" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "EVQ-PAG04M" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5900 4800
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5C2A70EA
P 6450 4350
F 0 "SW5" H 6450 4543 50  0000 C CNN
F 1 "SW_Push" H 6450 4544 50  0001 C CNN
F 2 "switches:SW_6x6_MULTI" H 6450 4550 50  0001 C CNN
F 3 "https://www3.panasonic.biz/ac/e_download/control/switch/light-touch/catalog/sw_lt_eng_5n.pdf" H 6450 4550 50  0001 C CNN
F 4 "SWITCH TACTILE SPST-NO 0.02A 15V" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "EVQ-PAG04M" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    6450 4350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5C2A7122
P 5350 4350
F 0 "SW1" H 5350 4543 50  0000 C CNN
F 1 "SW_Push" H 5350 4544 50  0001 C CNN
F 2 "switches:SW_6x6_MULTI" H 5350 4550 50  0001 C CNN
F 3 "https://www3.panasonic.biz/ac/e_download/control/switch/light-touch/catalog/sw_lt_eng_5n.pdf" H 5350 4550 50  0001 C CNN
F 4 "SWITCH TACTILE SPST-NO 0.02A 15V" H 0   0   50  0001 C CNN "Description"
F 5 "Panasonic Electronic Components" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "EVQ-PAG04M" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=P8009S-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5350 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5C2A71CB
P 6650 4400
F 0 "#PWR0112" H 6650 4150 50  0001 C CNN
F 1 "GND" H 6655 4227 50  0000 C CNN
F 2 "" H 6650 4400 50  0001 C CNN
F 3 "" H 6650 4400 50  0001 C CNN
	1    6650 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5C2A7461
P 6100 3950
F 0 "#PWR0113" H 6100 3700 50  0001 C CNN
F 1 "GND" H 6105 3777 50  0000 C CNN
F 2 "" H 6100 3950 50  0001 C CNN
F 3 "" H 6100 3950 50  0001 C CNN
	1    6100 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5C2A7482
P 6100 4400
F 0 "#PWR0114" H 6100 4150 50  0001 C CNN
F 1 "GND" H 6105 4227 50  0000 C CNN
F 2 "" H 6100 4400 50  0001 C CNN
F 3 "" H 6100 4400 50  0001 C CNN
	1    6100 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5C2A74A3
P 5550 4400
F 0 "#PWR0115" H 5550 4150 50  0001 C CNN
F 1 "GND" H 5555 4227 50  0000 C CNN
F 2 "" H 5550 4400 50  0001 C CNN
F 3 "" H 5550 4400 50  0001 C CNN
	1    5550 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5C2A74C4
P 6100 4850
F 0 "#PWR0116" H 6100 4600 50  0001 C CNN
F 1 "GND" H 6105 4677 50  0000 C CNN
F 2 "" H 6100 4850 50  0001 C CNN
F 3 "" H 6100 4850 50  0001 C CNN
	1    6100 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4850 6100 4800
Wire Wire Line
	6650 4400 6650 4350
Wire Wire Line
	6100 4400 6100 4350
Wire Wire Line
	6100 3950 6100 3900
Wire Wire Line
	5550 4400 5550 4350
Text GLabel 5700 3900 1    50   Output ~ 0
DP_UP
Text GLabel 5700 4350 1    50   Output ~ 0
DP_ENT
Text GLabel 5700 4800 1    50   Output ~ 0
DP_DOWN
Text GLabel 6250 4350 1    50   Output ~ 0
DP_RIGHT
Text GLabel 5150 4350 1    50   Output ~ 0
DP_LEFT
Text GLabel 2300 2750 2    50   Input ~ 0
DP_UP
Text GLabel 2300 2850 2    50   Input ~ 0
DP_DOWN
Text GLabel 2300 2950 2    50   Input ~ 0
DP_LEFT
Text GLabel 2300 3050 2    50   Input ~ 0
DP_RIGHT
Text GLabel 2300 3150 2    50   Input ~ 0
DP_ENT
Wire Wire Line
	1700 1500 1800 1500
Wire Wire Line
	1800 1500 1800 1550
Connection ~ 1700 1500
Wire Wire Line
	1700 1500 1700 1550
Text GLabel 2300 3350 2    50   Input ~ 0
~RESET
$Comp
L StepperTester-rescue:AVR-ISP-6-Connector J1
U 1 1 5C2AAE5B
P 3500 2000
F 0 "J1" H 3220 2096 50  0000 R CNN
F 1 "AVR-ISP-6" H 3220 2005 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" V 3250 2050 50  0001 C CNN
F 3 "https://drawings-pdf.s3.amazonaws.com/11636.pdf" H 2225 1450 50  0001 C CNN
F 4 "CONN HEADER VERT 6POS 2.54MM" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PRPC003DAAN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "S2011EC-03-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=S2011EC-03-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3500 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0117
U 1 1 5C2AAF57
P 3400 1450
F 0 "#PWR0117" H 3400 1300 50  0001 C CNN
F 1 "+5V" H 3415 1623 50  0000 C CNN
F 2 "" H 3400 1450 50  0001 C CNN
F 3 "" H 3400 1450 50  0001 C CNN
	1    3400 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5C2AAF7A
P 3400 2450
F 0 "#PWR0118" H 3400 2200 50  0001 C CNN
F 1 "GND" H 3405 2277 50  0000 C CNN
F 2 "" H 3400 2450 50  0001 C CNN
F 3 "" H 3400 2450 50  0001 C CNN
	1    3400 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1450 3400 1500
Wire Wire Line
	3400 2400 3400 2450
Text GLabel 3900 2000 2    50   Output ~ 0
SCK
Text GLabel 3900 2100 2    50   Output ~ 0
~RESET
Text GLabel 3900 1900 2    50   Output ~ 0
MOSI
Text GLabel 3900 1800 2    50   Input ~ 0
MISO
Connection ~ 1450 5850
Text GLabel 2500 7450 0    50   Output ~ 0
MISO
Text GLabel 2500 7250 0    50   Input ~ 0
CS
Text GLabel 2500 7550 0    50   Input ~ 0
MOSI
Text GLabel 2500 7350 0    50   Input ~ 0
SCK
Text GLabel 2500 7150 0    50   Output ~ 0
DIAG1
$Comp
L Connector_Generic:Conn_01x05 J3
U 1 1 5C2DA1BE
P 2700 7350
F 0 "J3" H 2779 7346 50  0000 L CNN
F 1 "Conn_01x05" H 2780 7301 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 2700 7350 50  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 2700 7350 50  0001 C CNN
F 4 "CONN HEADER VERT 5POS 2.54M" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PRPC005SAAN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "S1011EC-05-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=S1011EC-05-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    2700 7350
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7805 U4
U 1 1 5C2DBAB3
P 6050 5800
F 0 "U4" H 6050 6042 50  0000 C CNN
F 1 "L7805" H 6050 5951 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Horizontal_TabDown" H 6075 5650 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 6050 5750 50  0001 C CNN
F 4 "IC REG LINEAR 5V 1.5A TO220AB" H 0   0   50  0001 C CNN "Description"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "L7805ABV" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "497-2947-5-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=497-2947-5-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    6050 5800
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0119
U 1 1 5C2DBAE7
P 5750 5600
F 0 "#PWR0119" H 5750 5450 50  0001 C CNN
F 1 "+12V" H 5765 5773 50  0000 C CNN
F 2 "" H 5750 5600 50  0001 C CNN
F 3 "" H 5750 5600 50  0001 C CNN
	1    5750 5600
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0120
U 1 1 5C2DBB46
P 6350 5600
F 0 "#PWR0120" H 6350 5450 50  0001 C CNN
F 1 "+5V" H 6365 5773 50  0000 C CNN
F 2 "" H 6350 5600 50  0001 C CNN
F 3 "" H 6350 5600 50  0001 C CNN
	1    6350 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5C2DBB6D
P 6050 6150
F 0 "#PWR0121" H 6050 5900 50  0001 C CNN
F 1 "GND" H 6055 5977 50  0000 C CNN
F 2 "" H 6050 6150 50  0001 C CNN
F 3 "" H 6050 6150 50  0001 C CNN
	1    6050 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 6100 6050 6150
Wire Wire Line
	6350 5800 6350 5600
Wire Wire Line
	5750 5600 5750 5800
$Comp
L StepperTester-rescue:ATmega328-PU-MCU_Microchip_ATmega U1
U 1 1 5C2DF464
P 1700 3050
F 0 "U1" H 1300 4500 50  0000 R CNN
F 1 "ATmega328-PU" H 2350 1600 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 1700 3050 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 1700 3050 50  0001 C CNN
F 4 "IC MCU 8BIT 32KB FLASH 28DIP" H 1700 3050 50  0001 C CNN "Description"
F 5 "Microchip" H 1700 3050 50  0001 C CNN "Manufacturer"
F 6 "ATMEGA328-PU" H 1700 3050 50  0001 C CNN "Mfr PN"
F 7 "" H 1700 3050 50  0001 C CNN "Note"
F 8 "1" H 1700 3050 50  0001 C CNN "Qty Per Unit"
F 9 "" H 1700 3050 50  0001 C CNN "Critical?"
F 10 "" H 1700 3050 50  0001 C CNN "Subs OK?"
F 11 "ATMEGA328-PU-ND" H 1700 3050 50  0001 C CNN "Vendor 1 PN"
F 12 "https://www.digikey.com/products/en?keywords=atmega328-pu" H 1700 3050 50  0001 C CNN "Vendor 1 URL"
F 13 "" H 1700 3050 50  0001 C CNN "Vendor 2 PN"
F 14 "" H 1700 3050 50  0001 C CNN "Vendor 2 URL"
	1    1700 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack_Switch J4
U 1 1 5C2E17D9
P 5350 5900
F 0 "J4" H 5405 6125 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 5405 6126 50  0001 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-102AH_Horizontal" H 5400 5860 50  0001 C CNN
F 3 "https://www.cui.com/product/resource/digikeypdf/pj-102ah.pdf" H 5400 5860 50  0001 C CNN
F 4 "CONN PWR JACK 2X5.5MM SOLDER" H 0   0   50  0001 C CNN "Description"
F 5 "CUI" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PJ-102AH" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "CP-102AH-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=CP-102AH-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5350 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 6000 5700 6000
Wire Wire Line
	5650 5900 5700 5900
Wire Wire Line
	5700 5900 5700 6000
Connection ~ 5700 6000
$Comp
L Device:CP1 C1
U 1 1 5C2DB210
P 3550 5600
F 0 "C1" V 3802 5600 50  0000 C CNN
F 1 "100u" V 3711 5600 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 3550 5600 50  0001 C CNN
F 3 "http://www.rubycon.co.jp/en/catalog/e_pdfs/aluminum/e_ml.pdf" H 3550 5600 50  0001 C CNN
F 4 "CAP ALUM 100UF 20% 35V RADIAL" H 0   0   50  0001 C CNN "Description"
F 5 "Rubycon" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "35ML100MEFC8X7.5" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "1189-4176-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=1189-4176-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3550 5600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3350 5600 3400 5600
Wire Wire Line
	3350 5600 3350 5850
Wire Wire Line
	3350 5600 3350 5450
Connection ~ 3350 5600
$Comp
L power:GND #PWR02
U 1 1 5C2DCAEB
P 3750 5600
F 0 "#PWR02" H 3750 5350 50  0001 C CNN
F 1 "GND" H 3755 5427 50  0000 C CNN
F 2 "" H 3750 5600 50  0001 C CNN
F 3 "" H 3750 5600 50  0001 C CNN
	1    3750 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 5600 3750 5600
$Comp
L Mechanical:MountingHole H1
U 1 1 5C2EDCCA
P 7450 6100
F 0 "H1" H 7550 6100 50  0000 L CNN
F 1 "MountingHole" H 7550 6055 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7450 6100 50  0001 C CNN
F 3 "" H 7450 6100 50  0001 C CNN
	1    7450 6100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C2EDDC6
P 7450 6300
F 0 "H2" H 7550 6300 50  0000 L CNN
F 1 "MountingHole" H 7550 6255 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7450 6300 50  0001 C CNN
F 3 "" H 7450 6300 50  0001 C CNN
	1    7450 6300
	1    0    0    -1  
$EndComp
Text Notes 7200 5950 0    50   ~ 0
Mounting Holes
Wire Notes Line
	7150 5850 7150 6450
Wire Notes Line
	7150 6450 7850 6450
Wire Notes Line
	7850 6450 7850 5850
Wire Notes Line
	7850 5850 7150 5850
Wire Wire Line
	5700 6000 5700 6100
Wire Wire Line
	5650 5800 5750 5800
Connection ~ 5750 5800
Wire Wire Line
	5700 6100 6050 6100
Connection ~ 6050 6100
Text Notes 5550 5350 0    50   ~ 0
Power Supply
Text Notes 2300 1150 0    50   ~ 0
Microcontroller
Wire Notes Line
	1050 1000 4200 1000
Wire Notes Line
	4200 1000 4200 5000
Wire Notes Line
	4200 5000 1050 5000
Wire Notes Line
	1050 5000 1050 1000
Text Notes 2200 5250 0    50   ~ 0
Stepper Motor Driver
Wire Notes Line
	1050 7650 4200 7650
Wire Notes Line
	4200 7650 4200 5100
Wire Notes Line
	4200 5100 1050 5100
Wire Notes Line
	1050 5100 1050 7650
Text Notes 5400 1150 0    50   ~ 0
LCD
Wire Notes Line
	4550 1000 6900 1000
Wire Notes Line
	6900 1000 6900 3300
Wire Notes Line
	6900 3300 4550 3300
Wire Notes Line
	4550 3300 4550 1000
Text Notes 5600 3500 0    50   ~ 0
Keypad
Wire Notes Line
	4550 3400 6900 3400
Wire Notes Line
	6900 3400 6900 5150
Wire Notes Line
	6900 5150 4550 5150
Wire Notes Line
	4550 5150 4550 3400
Wire Notes Line
	4550 5250 6900 5250
Wire Notes Line
	6900 5250 6900 6450
Wire Notes Line
	6900 6450 4550 6450
Wire Notes Line
	4550 6450 4550 5250
$Comp
L Display_Character:WC1602A U3
U 1 1 5C2F5C9A
P 5650 2200
F 0 "U3" H 5400 2950 50  0000 C CNN
F 1 "WC1602A" H 5850 1450 50  0000 C CNN
F 2 "Display:WC1602A" H 5650 1300 50  0001 C CIN
F 3 "http://www.wincomlcd.com/pdf/WC1602A-SFYLYHTC06.pdf" H 6350 2200 50  0001 C CNN
F 4 "STANDARD LCD 16X2 + EXTRAS" H 0   0   50  0001 C CNN "Description"
F 5 "Adafruit" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "181" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "1528-1502-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/products/en?keywords=1528-1502-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 1700 5250 1700
Wire Wire Line
	6050 1600 6100 1600
Wire Wire Line
	6250 1350 6250 1450
Wire Wire Line
	6250 1450 6550 1450
Connection ~ 6250 1450
Wire Wire Line
	6550 1750 6550 1900
Wire Wire Line
	6550 1900 6050 1900
Wire Wire Line
	6250 2050 6250 2000
Wire Wire Line
	6050 2000 6250 2000
Connection ~ 6250 2000
Wire Wire Line
	6250 2000 6250 1750
Wire Wire Line
	5650 1350 5650 1400
Wire Wire Line
	5650 3000 5650 3050
NoConn ~ 5250 2100
NoConn ~ 5250 2200
NoConn ~ 5250 2300
NoConn ~ 5250 2400
NoConn ~ 2300 3250
NoConn ~ 2300 1850
NoConn ~ 2300 1950
NoConn ~ 2300 2050
NoConn ~ 1100 1850
$Comp
L Device:C C2
U 1 1 607835B3
P 2950 3500
F 0 "C2" H 3065 3546 50  0000 L CNN
F 1 "0.1" H 3065 3455 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D9.0mm_W5.0mm_P5.00mm" H 2988 3350 50  0001 C CNN
F 3 "~" H 2950 3500 50  0001 C CNN
	1    2950 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 60783F86
P 2950 3350
F 0 "#PWR01" H 2950 3200 50  0001 C CNN
F 1 "+5V" H 2965 3523 50  0000 C CNN
F 2 "" H 2950 3350 50  0001 C CNN
F 3 "" H 2950 3350 50  0001 C CNN
	1    2950 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 607843BE
P 2950 3650
F 0 "#PWR03" H 2950 3400 50  0001 C CNN
F 1 "GND" H 2955 3477 50  0000 C CNN
F 2 "" H 2950 3650 50  0001 C CNN
F 3 "" H 2950 3650 50  0001 C CNN
	1    2950 3650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
